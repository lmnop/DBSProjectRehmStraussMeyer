package databases;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, SQLException {
		// initialisierung und TreiberManagment.
		Connection con = null;
		Statement stmt = null;
		Class.forName("org.postgresql.Driver");
		con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Election", "postgres", "17castle");
		stmt = con.createStatement();
		boolean isretweet;
		boolean isquote;
		boolean trunc;
		// erstellen der Tabellen.
		String sqlcreatetabletweet = "CREATE TABLE IF NOT EXISTS tweetschema.tweet (tid integer NOT NULL DEFAULT nextval ('tweetschema.tweet_tid_seq'::regclass), handle character varying(15) COLLATE pg_catalog.default, text text COLLATE pg_catalog. default, isretweet boolean, originalauthor character varying(20) COLLATE pg_catalog.default, screenname character varying(20) COLLATE pg_catalog.default, isquote boolean, retweetcount integer, favoritecount integer, sourceurl character varying(50) COLLATE pg_catalog.default, truncated boolean, date timestamp(6) without time zone, CONSTRAINT tweet_pkey PRIMARY KEY (tid)) WITH (OIDS = FALSE) TABLESPACE pg_default; ALTER TABLE tweetschema.tweet OWNER to postgres;";
		String sqlcreatetabletags = "CREATE TABLE IF NOT EXISTS tweetschema.hashtags(tid integer NOT NULL, name character varying(40) COLLATE pg_catalog.default NOT NULL, CONSTRAINT hashtags_pkey PRIMARY KEY (tid, name)) WITH (OIDS = FALSE) TABLESPACE pg_default; ALTER TABLE tweetschema.hashtags OWNER to postgres;";
		stmt.executeUpdate(sqlcreatetabletweet);
		stmt.executeUpdate(sqlcreatetabletags);
		// zum auslesen der .csv Datei.
		String[] oneline = new String[11];
		Scanner scanner = new Scanner(new File("data.csv"));
		scanner.useDelimiter("\r\n");
		int tid = 0;
		// verwirft erste Zeile
		if (scanner.hasNext()) {
			String kat = scanner.next();
			System.out.println("Kategorien sind:" + kat);
		}
		// lie�t alle weiteren Zeilen der .csv Datei.
		while (scanner.hasNext()) {
			String line = scanner.next();
			System.out.println(line);
			String[] split = line.split(";");
			// Verwerfe alle Tweets die mit \r\n geschrieben wurden.(ca 150)
			if (split.length != 11) {
				continue;
			}
			// wir benutzen string-Konkatenation (anf�llig f�r injektion
			// :C), deswegen entfernen wir alle '.
			split[1] = split[1].replace("'", "");
			System.out.println(tid);
			tid++;
			// Datentypen anpassen, wobei uns aufgefallen ist, dass psql das
			// auch teilweise selber kann.
			if (split[2] == "True") {
				isretweet = true;
			} else {
				isretweet = false;
			}
			if (split[6] == "True") {
				isquote = true;
			} else {
				isquote = false;
			}
			int rtcount = Integer.parseInt(split[7]);
			int favcount = Integer.parseInt(split[8]);

			if (split[10] == "True") {
				trunc = true;
			} else {
				trunc = false;
			}
			// schicken der Queries f�r die Tabelle tweet
			String sqltweet = "INSERT INTO tweetschema.tweet (handle,text,isretweet,originalauthor,screenname,isquote,retweetcount,favoritecount,sourceurl,truncated,tid,date)"
					+ "VALUES('" + split[0] + "','" + split[1] + "','" + isretweet + "','" + split[3] + "','"
					+ split[5] + "','" + isquote + "','" + rtcount + "','" + favcount + "','" + split[9] + "','" + trunc
					+ "','" + tid + "','" + split[4] + "');";

			stmt.executeUpdate(sqltweet);
			// einlesen der Hashtags
			Scanner wortscanner = new Scanner(split[1]);
			wortscanner.useDelimiter(" |\n");
			while (wortscanner.hasNext()) {
				String wort = wortscanner.next();
				// Hashtags in die Tabelle hashtags schreiben
				if (wort.contains("#")) {
					try{
					String sqltags = "INSERT INTO tweetschema.hashtags (tid,name) VALUES('" + tid + "','" + wort
							+ "');";
					stmt.executeUpdate(sqltags);
					}
					//sollte ein hashtag doppelt im tweet vorkommen, wird dieser hashtag ignoriert
					catch(org.postgresql.util.PSQLException e){System.out.println(e);}
				}
			}
		}
		stmt.close();
		con.close();
		scanner.close();

	}

}
/*
 * Anpassungen:
 * 1. wir haben auf eine #id verzichtet, da wir doppelte tags abfangen und somit tid und name als Prim�rschl�ssel ausreichend sind.
 * 2. praktisch ist Timestamp hier eine 1:1 Relation. Wir haben ihn deshalb gleich in die Tabelle tweet mit aufgenommen.
 * 3. die einzigen Autoren sind Trump und Clinton, deshalb haben wir auch von unserem Entit�tstypen Account abgelassen.
 * 4. die rekusive definition von Retweet ist nicht m�glich, da der Original-Tweet nicht in der Datenbank vorhanden ist. Der Original-Autor ist jetzt ein Attribut von Tweet.
 */

from flask import Flask
from flask import render_template
from flask import request
import psycopg2
import itertools
import random
import datetime
app = Flask(__name__)

connection = psycopg2.connect(
	"host='192.168.179.187' dbname='twitter' user='postgres' password='postgres'")
cursor = connection.cursor()

@app.route('/')
@app.route('/index')
def index():
	cursor.execute("SELECT tid, name FROM tweetschema.hashtags")
	result = cursor.fetchall()
	
	graph = { 'nodes': [], 'edges': []}
	hashtags = []
	tweet = {}
	numTag = 0
	
	# Für jeden Hashtag einen Knoten erstellen
	for (tid, name) in result:
		if name not in hashtags:
			hashtags.append(name)
			graph.get('nodes').append({
				'id': 'n' + str(len(hashtags) - 1),
				'label': name,
				'x': random.random(),
				'y': random.random(),
				'size': 1,
				'color': '#008000'
			})
		# Für jeden Tweet ein Eintrag im Dictionary
		if tid not in tweet:
			tweet[tid] = []
		# Im Dictionary die zusammenhänge zw. hashtag und tweet speichern
		tweet[tid].append(name)
	# Für jede zwei hashtags in einem tweet eine Kante
	for tid in tweet:
		for (tag1, tag2) in itertools.combinations(tweet[tid], 2):
			graph.get('edges').append({
				'id': 'e' + str(numTag),
				'source': 'n' + str(hashtags.index(tag1)),
				'target': 'n' + str(hashtags.index(tag2)),
				'size': 1,
				'color': '#ccc'
			})
			numTag += 1
	
	data = []
	date = datetime.date(2016, 1, 5)
	last = datetime.date(2016, 9, 28)
	# Für jeden Tag die Anzahl der hashtags abfragen
	while date <= last:
		today = date.strftime("%Y-%m-%d") # Format in der db
		date = date + datetime.timedelta(days=1)
		tomorrow = date.strftime("%Y-%m-%d")
		cursor.execute("select count(*) as exact_count from tweetschema.tweet, tweetschema.hashtags where tweet.tid = hashtags.tid and date >= '" + today + "' and date < '" + tomorrow + "'")
		result=cursor.fetchall()
		data.append(result[0])
	
	tag = request.args.get('hashtag') # der Hashtag steht als argument in der url
	if tag is None: # z.b. /index
		tag = ""
	data2 = []
	date = datetime.date(2016, 1, 5)
	# Für jeden Tag die Anzahl der Vorkomnisse des hashtags abfragen
	while date <= last:
		today = date.strftime("%Y-%m-%d")
		date = date + datetime.timedelta(days=1)
		tomorrow = date.strftime("%Y-%m-%d")
		cursor.execute("select count(*) as exact_count from tweetschema.tweet, tweetschema.hashtags where tweet.tid = hashtags.tid and date >= '" + today + "' and date < '" + tomorrow + "' and name = '" + tag + "'")
		result=cursor.fetchall()
		data2.append(result[0])
	
	return render_template("index.html",
                           title='Home',
						   graph=graph, # Der Graph
						   occ=data,    # Die Daten für das erste Diagramm 
						   occ2=data2)  # zweites Diagramm

app.run(debug=True)
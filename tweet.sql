PGDMP                         u           Election    9.6.3    9.6.3 	    Q           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            R           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �            1259    16577    tweet    TABLE     x  CREATE TABLE tweet (
    tid integer NOT NULL,
    handle character varying(15),
    text text,
    isretweet boolean,
    originalauthor character varying(20),
    screenname character varying(20),
    isquote boolean,
    retweetcount integer,
    favoritecount integer,
    sourceurl character varying(50),
    truncated boolean,
    date timestamp(6) without time zone
);
    DROP TABLE tweetschema.tweet;
       tweetschema         postgres    false            �            1259    16575    tweet_tid_seq    SEQUENCE     o   CREATE SEQUENCE tweet_tid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE tweetschema.tweet_tid_seq;
       tweetschema       postgres    false    187            S           0    0    tweet_tid_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE tweet_tid_seq OWNED BY tweet.tid;
            tweetschema       postgres    false    186            �           2604    16580 	   tweet tid    DEFAULT     X   ALTER TABLE ONLY tweet ALTER COLUMN tid SET DEFAULT nextval('tweet_tid_seq'::regclass);
 =   ALTER TABLE tweetschema.tweet ALTER COLUMN tid DROP DEFAULT;
       tweetschema       postgres    false    186    187    187            N          0    16577    tweet 
   TABLE DATA               �   COPY tweet (tid, handle, text, isretweet, originalauthor, screenname, isquote, retweetcount, favoritecount, sourceurl, truncated, date) FROM stdin;
    tweetschema       postgres    false    187   �	       T           0    0    tweet_tid_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('tweet_tid_seq', 11924, true);
            tweetschema       postgres    false    186            �           2606    16585    tweet tweet_pkey 
   CONSTRAINT     H   ALTER TABLE ONLY tweet
    ADD CONSTRAINT tweet_pkey PRIMARY KEY (tid);
 ?   ALTER TABLE ONLY tweetschema.tweet DROP CONSTRAINT tweet_pkey;
       tweetschema         postgres    false    187    187            N      x������ � �     